// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "IBMSDK",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "IBMSDK",
            targets: ["IBMSDK"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "IBMSDK",
            dependencies: ["BinaryLib"],
            path: "Sources"),
        .binaryTarget(name: "BinaryLib", path: "BinaryLib/FRBDeviceHealthSDK.xcframework")
    ]
)
