//
//  FRBDeviceHealthSDK.h
//  FRBDeviceHealthSDK
//
//  Created by Vinayakam on 13/01/22.
//

#import <Foundation/Foundation.h>
#import "SdkHandler.hpp"

//! Project version number for FRBDeviceHealthSDK.
FOUNDATION_EXPORT double FRBDeviceHealthSDKVersionNumber;

//! Project version string for FRBDeviceHealthSDK.
FOUNDATION_EXPORT const unsigned char FRBDeviceHealthSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FRBDeviceHealthSDK/PublicHeader.h>


