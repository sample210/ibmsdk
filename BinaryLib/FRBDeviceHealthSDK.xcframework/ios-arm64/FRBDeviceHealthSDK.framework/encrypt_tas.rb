require 'openssl'

############# Constants ##############
FAT_MAGIC=0xcafebabe
#single architecture binary (not fat)
#Magics taken from http://ilostmynotes.blogspot.co.il/2014/05/mach-o-filetype-identification.html
LE32_MAGIC=0xcefaedfe #Mach-O Little Endian (32-bit)
LE64_MAGIC=0xcffaedfe #Mach-O Little Endian (64-bit)
BE32_MAGIC=0xfeedface #Mach-O Big Endian (32-bit)
BE64_MAGIC=0xfeedfacf #Mach-O Big Endian (64-bit)

MARKER_START_UNENCRYPTED=0
MARKER_START_SIZE=64

#Machine type IDs used in CPU type encoding.
CTM_i386=7
CTM_ARM=12
CTM_ARM64=16777228
CTM_x86_64=16777223

#ARM Machine Subtypes
CSARM_ALL    = 0
CSARM_V4T    = 5
CSARM_V6     = 6
CSARM_V5TEJ  = 7
CSARM_XSCALE = 8
CSARM_V7     = 9
CSARM_V7F    = 10
CSARM_V7S    = 11
CSARM_V7K    = 12

#ARM64 Machine Subtypes
CPU_SUBTYPE_ARM64_ALL = 0


#x86 Machine Subtypes.
CSX86_ALL = 3

############# Binary structure variables ################
$LC_SEGMENT = 0x1
$HDR_SIZE = 7
$SECTION_HDR_SIZE = 8
$SECTION_HDR_SUBSECTIONS = 6
$SECTION_HDR_SUBSECTION_SIZE = 9
$ADDRESS_SIZE = 4
$ADDRESS_SIZE_FORMAT = "V"

############# Logging ################

def log(msg_)
  puts msg_
end

def logv(msg_)
  puts msg_ if $verbose
end

############ File functions #########
class File
  def read_int32
    ret = self.read(4)
    ret = ret.unpack("N")[0] 
  end
end


# Retrive the first line un a file that contains the given string
#
# @param file [String] the path to the file
# @param string [String] the string to search for
# @return  [String] the first line that contains the given synbol or 'nil' if symmbol was not found
def get_line_in_file( file, string )
  File.open( file ) do |io|
    io.each {|line| line.chomp! ; return line if line.include? string}
  end
  nil
end

def set_64_args()
  $LC_SEGMENT = 0x19
  $HDR_SIZE = 8
  $SECTION_HDR_SIZE = 12
  $SECTION_HDR_SUBSECTIONS = 10
  $SECTION_HDR_SUBSECTION_SIZE = 12
  $ADDRESS_SIZE = 8
  $ADDRESS_SIZE_FORMAT = "Q"
end

# Retrive the cpu type and subype for the given map file architecture
#
# @param map_ [String] the path to the linker generated map file
# @return  [Fixnum, Fixnum] the cpu type and cpu subtype of the given map file architecture
def get_map_cpu_subtype(map_)
  arch_line = get_line_in_file(map_, "# Arch:")
  fail "Couldn't get map file architecture" unless arch_line
  arc = (arch_line.split(" ")[2])
  log "Map file architecture: #{arc}"
  case arc
    when "armv6"
      cputype, cpusubtype = CTM_ARM,CSARM_V6
    when "armv7"
      cputype, cpusubtype = CTM_ARM,CSARM_V7
    when "armv7s"
      cputype, cpusubtype = CTM_ARM,CSARM_V7S
    when "i386"
      cputype, cpusubtype = CTM_i386,CSX86_ALL
    when "arm64"
	  set_64_args()
      cputype, cpusubtype = CTM_ARM64,CPU_SUBTYPE_ARM64_ALL
    when "x86_64"
	  set_64_args()
	  cputype, cpusubtype = CTM_x86_64,CSX86_ALL
    else
      fail "Unknown map file architecture: #{arc}"
  end
end

# Retrive the "__data" section relative address from a given segment header.
#
# @param io_ [IO] An open I/O stream offseted to a specific segment header
# @return  [Fixnum] the "__data" section relative address
def get__data_section_relative_address(io_)
  num_sections = io_.read($SECTION_HDR_SIZE*4).unpack("VVVVVVVVVVVV")[$SECTION_HDR_SUBSECTIONS]
  while num_sections > 0
    sect_name = io_.read(16)[/^.+?\0/]
    sect_name.chop! if sect_name
	num_sections = num_sections - 1
    if (sect_name == "__data")
      io_.seek(16*1, IO::SEEK_CUR)
      address = io_.read($ADDRESS_SIZE).unpack($ADDRESS_SIZE_FORMAT)[0]
	  io_.read($ADDRESS_SIZE)
      offset = io_.read(4).unpack("V")[0]
      return offset - address
    end
    io_.seek(16+$SECTION_HDR_SUBSECTION_SIZE*4, IO::SEEK_CUR)
  end
  return 0
end

# Get the architecture offset in the given binary that matches the archticture given
#
# @param io_ [IO] An open I/O stream to a binary with "r" permissions
# @param cpu_type_ [Fixnum] the cpu type of the current architecture (e.g CTM_ARM)
# @param cpu_subtype_ [Fixnum] the cpu subtype of the current architecture (e.g CSARM_V7)
# @return  [Fixnum] the architecture offset
def get_archs_offsets(io_, cpu_type_, cpu_subtype_)
  # Read magic to identify if this is a fat file
     fat_file_sig = io_.read_int32

     case fat_file_sig
       when FAT_MAGIC
         fat_file_n_archs = io_.read_int32
       
         while fat_file_n_archs > 0
          arch_data = io_.read(5*4).unpack("NNNNN")
          if arch_data[0] == cpu_type_ and arch_data[1] == cpu_subtype_
            return arch_data[2]
          end
          fat_file_n_archs = fat_file_n_archs -1
         end
       when LE32_MAGIC,LE64_MAGIC,BE32_MAGIC,BE64_MAGIC
         return 0
       else
         throw "Invalid file header signature 0x#{fat_file_sig.to_s(16)}"
     end
   fail "map file architecture is not present in binary"
end

# Get the "__DATA _data" section base address in the given binary that matches the archticture given
#
# @param binary_ [String] the path to the mach-o binary to encrypt
# @param cpu_type_ [Fixnum] the cpu type of the current architecture (e.g CTM_ARM)
# @param cpu_subtype_ [Fixnum] the cpu subtype of the current architecture (e.g CSARM_V7)
# @return  [Fixnum] "__DATA _data" section base address or 0 if segment or section is not found
def get__DATA__data_section_base_address(binary_, cpu_type_, cpu_subtype_)
   
  File.open(binary_, "r") do |appfile|

  ofs = get_archs_offsets(appfile, cpu_type_, cpu_subtype_)
  appfile.seek(ofs)

  hdr = appfile.read($HDR_SIZE*4).unpack("VVVVVVV")
  ncmd = hdr[4]
  cmdsize = hdr[5]

    while ncmd > 0
       bytes_read = 0
       cmdhdr = appfile.read(2*4).unpack("VV")
       bytes_read += 2*4
       cmdlen = cmdhdr[1]
       if cmdhdr[0] == $LC_SEGMENT
         segname = appfile.read(16)[/^.+?\0/]
         segname.chop! if segname
         bytes_read += 16
         if (segname == "__DATA")
            return get__data_section_relative_address(appfile) + ofs
         end
       end
       
       appfile.seek(cmdlen-bytes_read, IO::SEEK_CUR)
       
       ncmd = ncmd - 1
     end   

  end
   
   return 0
end

# Get the marker_start and marker_end offsets based on the map file
#
# @param map_ [String] the path to the linker generated map file
# @return     [Fixnum, Fixnum] The marker_start and marker_end offsets
def get_start_end_offsets(map_)
  search_end = false
  start_address = 0
  end_address = 0
  data_addr = 5

  File.open( map_ ) do |io|
    io.each do |line|
     line.chomp!
     #logv line
     if search_end
      if line.include? "marker_end"
        logv line
        end_address = Integer(line.split(" ")[0])
        break
      end
     else
      if line.include? "marker_start"
        logv line
        start_address = Integer(line.split(" ")[0])
        #now search for the end marker
        search_end = true
      end
     end
    end
  end
  fail "Couldn't find markers" if (start_address == 0) or (end_address == 0)
  return start_address , end_address
end

############ Encryption functions ############
# Encrypt data based on given offsets
# This will encrypt all data between [base_+start_+4..base_+end_]
#
# @param binary_  [String] the path to the binary to encrypt
# @param base_    [Fixnum] the base address
# @param start_   [Fixnum] The offset from the base address of the marker_start
# @param end_     [Fixnum] The offset from the base address of the marker_end
def encrypt_data(binary_, base_, start_, end_)
    offset = base_ + start_ + MARKER_START_SIZE
    content_size = end_ - start_ - MARKER_START_SIZE
	sek = base_ + start_
    File.open(binary_, "r+b") do |file|
        file.seek(base_ + start_)
        marker_value = file.read(MARKER_START_SIZE).unpack("N")[0]
        
        # Don't encrypt if initial value is wrong
        if marker_value == MARKER_START_UNENCRYPTED
            file.seek(offset)
            data, key, iv = encrypt_data_aes(file, content_size)

            #write to file
            file.seek(base_ + start_)
            if !$dryrun
                file.write(key)
                #content size
                file.write(Array(data.size + iv.size).pack("l"))
                #padding
                file.seek(12, IO::SEEK_CUR)
                file.write(iv)
				file.write(data)
            end
            log "Encryption done"
        else
            log "WARNING: binary appear to be already encrypted. Skipping encryption."
        end
    end
end

# Encrypt data using aes
#
# @param file_          [IO] An open I/O stream to the data to encrypt
# @param size_          [Fixnum] Size of the data to encrypt
# @return data, key, iv [String, String, String] The encrypted data, key used, iv used
#
def encrypt_data_aes(file_, size_)
    data = ""
    e = OpenSSL::Cipher::Cipher.new('aes256').encrypt
    key = e.random_key
    iv = e.random_iv
    
    data << e.update(file_.read(size_))
    puts "data size: #{data.size}"
    data << e.final

    log "type: #{data.class}"
    return data, key, iv
end

# Encrypt the "__DATA __data" TAS section in the mach-o binary for a single architecture
# Architecture is retrived from the mapfile 
#
# @param binary_  [String] the path to the mach-o binary to encrypt
# @param map_     [String] the path to the linker generated map file
# Generate a map file via the xcode build settings: "Write Link Map File"
#   or by specificng "-map <MAP_FILE>" to ld
def encrypt_data_section(binary_, map_)
  fail "Can't find binary file: #{binary_}" if !File.exist?(binary_)
  fail "Can't find map file: #{map_}" if !File.exist?(map_)
  
  log "Encrypting #{binary_} with map file #{map_}"

  arch_cpu, arch_cpu_subtype = get_map_cpu_subtype(map_)
  marker_start_offset, marker_end_offset = get_start_end_offsets(map_)
  arc_base = get__DATA__data_section_base_address(binary_, arch_cpu, arch_cpu_subtype)
  encrypt_data(binary_, arc_base, marker_start_offset, marker_end_offset)
end

################## main ########################
if ARGV.size<2 then
  puts "Usage: #{$0} <BINARY_FILE> <MAP_FILE>"
  exit
end

if ARGV.size>3 and ARGV[3]=="-n" then
    $dryrun = true
end

if ARGV.size>2 and ARGV[2]=="-v" then
  $verbose = true
end

encrypt_data_section(ARGV[0], ARGV[1])
